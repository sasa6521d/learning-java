package MyPackage;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Main {
    record Person(String name, int age, double savings) {
    }

    record GreetResponse(
            String greet,
            List<String> favProgLangs,
            Person person) {
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);

    }

    // @GetMapping("/greet")
    // public GreetResponse greet() {

    // GreetResponse response = new GreetResponse("Bello", List.of("Java", "Go",
    // "JS", "Py"),
    // new Person("Mado", 55, 30_000));

    // return response;
    // }

    // create a method that takes the name from the request and returns a greeting
    // the url will look like http://localhost:8080/greet?name=John
    // the response will be a html page with the text "Hello John" styled with h1
    // tag
    @GetMapping("/greet")
    public String greet(String name) {
        return "<h1 style='color:red;'>Hello " + name + "</h1>";
    }

}